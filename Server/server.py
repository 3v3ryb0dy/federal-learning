import os
from flask import Flask, jsonify, request, send_from_directory
import tensorflow as tf
import numpy as np
from flask_cors import CORS, cross_origin
from dotenv import load_dotenv
import json
import tensorflowjs as tfjs


# 3D Array [
# [[weigths_layer1],[bias_layer1],[weights_layer3],[bias_layer3]],
# [[weigths_layer1],[bias_layer1],[weights_layer3],[bias_layer3]]
# ]
class Wrapper():
    weight_sets = []


wrapper = Wrapper()
load_dotenv()
MODEL_PATH_PY = os.getenv("MODEL_PATH_PY")
MODEL_PATH_PY_UNTOUCHED = os.getenv("MODEL_PATH_PY_UNTOUCHED")
MODEL_PATH_JS = os.getenv("MODEL_PATH_JS")
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


def adjustWeights():
    print("Adjusting weights!")
    model = tf.keras.models.load_model(MODEL_PATH_PY)
    client_weight_sum = 0
    for weight_set in wrapper.weight_sets:
        client_weight_sum += weight_set

    client_weight_mean = client_weight_sum / len(wrapper.weight_sets)

    current_weights_layer1 = np.array(model.layers[0].get_weights())
    current_weights_layer3 = np.array(model.layers[3].get_weights())
    current_weights_layer5 = np.array(model.layers[5].get_weights())

    layer1 = ((current_weights_layer1) + (client_weight_mean[0] * 2)) / 3
    layer3 = ((current_weights_layer3) + (client_weight_mean[1] * 2)) / 3
    layer5 = ((current_weights_layer5) + (client_weight_mean[2] * 2)) / 3

    model.layers[0].set_weights(layer1)
    model.layers[3].set_weights(layer3)
    model.layers[5].set_weights(layer5)

    model.save(MODEL_PATH_PY)
    tfjs.converters.save_keras_model(model, MODEL_PATH_JS)

    wrapper.weight_sets = []


@app.route("/predict", methods=["POST"])
@cross_origin()
def predict():
    model = tf.keras.models.load_model(MODEL_PATH_PY)
    tensor = np.array(request.json["matrix"]).reshape(1, 28, 28, 1)
    prediction = np.argmax(model.predict(tensor))
    return str(prediction), 201


@app.route("/download/<path:filename>")
@cross_origin()
def getFile(filename):
    return send_from_directory(MODEL_PATH_JS, filename)


@app.route("/updateWeights", methods=["POST"])
@cross_origin()
def updateWeights():
    model = tf.keras.models.load_model(MODEL_PATH_PY)

    weights = np.array([
        np.array([
            np.array(request.json["weights"][0][0]), 
            np.array(request.json["weights"][0][1])
        ]),
        np.array([
            np.array(request.json["weights"][1][0]), 
            np.array(request.json["weights"][1][1])
        ]),
        np.array([
            np.array(request.json["weights"][2][0]), 
            np.array(request.json["weights"][2][1])
        ])
    ])
    wrapper.weight_sets.append(weights)
    print(len(wrapper.weight_sets))
    if len(wrapper.weight_sets) > 4:
        adjustWeights()
    return "Succes", 201

@app.route("/read", methods=["GET"])
@cross_origin()
def asdas():
    model = tf.keras.models.load_model(MODEL_PATH_PY)
    print(model.summary())

if __name__ == "__main__":
    app.run(debug=True)
