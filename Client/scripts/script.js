let mousePressed = false
let lastX, lastY
let canvas
let ctx
let matrices = []
let model
let labels = []

function InitThis() {
  canvas = document.getElementById('myCanvas')
  ctx = canvas.getContext('2d')

  $('#myCanvas').mousedown(function(e) {
    mousePressed = true
    Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, false)
  })

  $('#myCanvas').mousemove(function(e) {
    if (mousePressed) {
      Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, true)
    }
  })

  $('#myCanvas').mouseup(function(e) {
    mousePressed = false
  })
  $('#myCanvas').mouseleave(function(e) {
    mousePressed = false
  })
}

function Draw(x, y, isDown) {
  if (isDown) {
    ctx.beginPath()
    ctx.strokeStyle = 'black'
    ctx.lineWidth = 16
    ctx.lineJoin = 'round'
    ctx.moveTo(lastX, lastY)
    ctx.lineTo(x, y)
    ctx.closePath()
    ctx.stroke()
  }
  lastX = x
  lastY = y
}

function clearArea() {
  ctx.setTransform(1, 0, 0, 1, 0, 0)
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
}

function getLabel() {
  return parseInt(document.getElementById('label').value)
}

async function compress() {
  const width = 28
  const height = 28
  let imgData
  let imgDataArr

  const label = getLabel()

  labels = [...labels, label]

  let img = new Image()
  img.src = canvas.toDataURL()
  img.className = 'inputImg'
  document.body.appendChild(img)

  let span = document.createElement('span')
  span.style = 'font-size: 10pt;'
  span.textContent = `Label: ${label}`
  document.body.appendChild(span)

  return new Promise((res, rej) => {
    let first = true
    img.onload = () => {
      if (first) {
        first = false

        const elem = document.createElement('canvas')
        elem.width = width
        elem.height = height

        const ctx = elem.getContext('2d')
        ctx.drawImage(img, 0, 0, width, height)

        imgData = ctx.getImageData(0, 0, 28, 28)
        imgDataArr = Array.from(imgData.data.filter((_, i) => i % 4 === 3))
        img.src = elem.toDataURL()

        let helperMatrix = []
        while (imgDataArr.length) helperMatrix.push(imgDataArr.splice(0, 28))

        const matrix = helperMatrix.map((row) => row.map((value) => value / 255))

        matrices = [...matrices, matrix]

        predict(matrix).then(console.log)
        elem.remove()

        res(matrix)
      }
    }
  })
}

function submit(matrix) {
  let xhr = new XMLHttpRequest()
  xhr.open('POST', 'http://127.0.0.1:5000/predict', true)
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.setRequestHeader('Access-Control-Allow-Origin', '*')
  xhr.send(
    JSON.stringify({
      matrix,
      label: labels[labels.length - 1]
    })
  )
  xhr.onload = () => {
    let res = xhr.response
    let span = document.createElement('span')
    span.style = 'font-size: 10pt;'
    span.textContent = `Prediction: ${res}`
    document.body.appendChild(span)
  }
}

async function getModel() {
  console.log(model)
}

async function predict(matrix) {
  model = await tf.loadLayersModel('http://127.0.0.1:5000/download/model.json')
  let tensor = await tf.tensor([matrix])
  tensor = tensor.reshape([1,28,28,1])
  return new Promise((res, rej) => {
    model
      .predict(tensor)
      .array()
      .then((predictions) => {
        const prediction = predictions[0]
        res(prediction.indexOf(Math.max.apply({}, prediction)))
      })
  })
}

async function getModelWeights(model) {
  const layerWeights = await Promise.all(
    model.layers.map(async (l, i) => {
      if (l.weights.length) {
        const weights = await model.layers[i].getWeights()[0].array()
        const biases = await model.layers[i].getWeights()[1].array()
        return [weights, biases]
      }
      return null
    })
  )
  const l= layerWeights.filter((e) => e !== null)
  console.log(l)
  return l
}

async function trainModel() {
  const epochs = 5
  const tensor = await tf.tensor(matrices).reshape([matrices.length, 28, 28, 1])
  const labelTensor = await tf.tensor(labels)

  model = await tf.loadLayersModel('http://127.0.0.1:5000/download/model.json')

  await model.compile({
    optimizer: 'adam',
    loss: 'sparseCategoricalCrossentropy',
    metrics: ['accuracy']
  })

  console.log(`Training ${epochs} epochs with ${matrices.length} samples...`)
  await model.fit(tensor, labelTensor, { epochs }).then((info) => {
    console.log('Accuracy: ' + info.history.acc)
  })

  // weights in format [[[weights layer1], [bias layer1]], [[weights layer3], [bias layer3]]]
  const weights = getModelWeights(model)

  return weights
}

function saveWeights(weights) {
  console.log(weights)
  let url = 'http://127.0.0.1:5000/updateWeights'
  fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    referrerPolicy: 'no-referrer',
    body: JSON.stringify({
      weights
    })
  })
}
